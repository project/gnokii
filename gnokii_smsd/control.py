#!/usr/bin/env python
# This is the daemon start/stop script
###########################################################################

# Define some names
NAME = 'gnokii-smsd'

import config
from gnokii_smsd import SMSDaemon

###########################################################################

import sys, os
import os.path
import signal

def start_daemon():
  try:
    lockfile = open(LOCKFILE, 'r').read(1000)
    sys.stderr.write("Another process is already running.")
  except:
    # do the UNIX double-fork magic, see Stevens' "Advanced
    # Programming in the UNIX Environment" for details (ISBN 0201563177)
    try:
      pid = os.fork()
      if pid > 0:
        # exit first parent
        sys.exit(0)
    except OSError, e:
      sys.stderr.write("fork #1 failed: %d (%s)" % (e.errno, e.strerror))
      sys.exit(1)

    # decouple from parent environment
    os.chdir("/")   #don't prevent unmounting....
    os.setsid()
    os.umask(0)

    # do second fork
    try:
      pid = os.fork()
      if pid > 0:
        # exit from second parent, print eventual PID before
        print pid
        open(LOCKFILE,'w').write("%d" % pid)
        sys.exit(0)
        
    except OSError, e:
      sys.stderr.write("fork #2 failed: %d (%s)" % (e.errno, e.strerror))
      sys.exit(1)

    # start the daemon main loop
    SMSDaemon().main()

def stop_daemon():
  try:
    pid = file(LOCKFILE).read()
    print "Stopping daemon process [%s]" % (pid)
    # Try to exit with some clean up
    os.kill(int(pid), signal.SIGTERM)
    # TODO Force exit if not
    print "Process stopped"
  except IOError:
    sys.stderr.write("Cannot read pid from the lock file: [%s] " % (LOCKFILE))
  except OSError, e:
    sys.stderr.write("Cannot stop daemon process %d, %s" % (e.errno, e.strerror))
  # Whatever remove lock file if exists
  if os.path.exists(LOCKFILE):
    os.unlink(LOCKFILE)

def stat_daemon():
  if os.path.exists(LOCKFILE):
    pid = int(file(LOCKFILE).read())
    os.system("ps -F --pid %d" % (pid))
    # Try top, cumulative time mode
    # os.system("top -n1 -S -p %d" % (pid))
  else:
    print "Daemon process not running"

if __name__ == "__main__":
  # Get instance init settings
  LOCKFILE = config.settings.get('main', 'lock_file')

  if len(sys.argv) >= 2:
    command = sys.argv[1]
  else:
    command =  'start'
  
  if  command == 'start':
    print "Starting daemon: " + NAME
    start_daemon()
  elif command == 'stop':
    print "Stopping daemon: " + NAME
    stop_daemon()
  elif command == 'status':
    print "Retrieveing status from daemon: " + NAME
    stat_maggie()
  elif command == 'restart':
    print "Restarting  daemon: " + NAME
    stop_daemon()
    start_daemon()
  else:
    print "Usage control.py [start|stop|restart|status]"  
    sys.exit(0)