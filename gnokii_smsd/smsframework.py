#!/usr/bin/env python
# encoding: utf-8
#
# Drupal smsframework webservice

import re, httplib, urllib
import logging

import config

class smsframework(object):
  """ This will handle communications in and out Drupal + smsframework 
      @todo Handle opening and closing connections
      Persistent connections will be good to handle multiple batch sends
  """
  
  def __init__(self):
    self.log = logging.getLogger()
    """ Create webservice using config from settings files """
    self.host = config.settings.get('smsframework', 'host')
    self.path_in = config.settings.get('smsframework', 'path_incoming')
    self.path_out = config.settings.get('smsframework', 'path_outgoing')
    self.port = config.settings.getint('smsframework', 'port')   
    # Now create web service with all these settings
    self.response = None
    self.conn = None
    
  def sms_in(self, sms):
    """ Process incoming SMS, returns True if success or False otherwise """
    number = sms['number']
    message = sms['message']
    """ Send message to webservice and get response """
    #params = urllib.urlencode({'number': number, 'message': message})
    params = {'number': number, 'message': message}
    statuscode = self.post(self.path_in, params)
    if statuscode == 200:
      return True
    else:
      return False
  
  def sms_out(self):
    """ Fetch ougtoing SMSs for sending """
    statuscode = self.get(self.path_out)
    # Get reply and process
    if statuscode == 200:
      # number=2035125136&message=Framework Testing confirm code: 4989
      res = self.response.read()
      reg = re.compile("number=(\d+)&message=(.+)")
      matches = reg.match(res)
      if matches:
        sms = {}
        sms['number'] = matches.group(1)
        sms['message'] = matches.group(2)
        return sms
    
    # Default case, return none
    return None

  def get(self, path, params = {}):
    """ Get from given path """
    self.log.debug('GET: %s:%d%s' % (self.host, self.port, path));

    self.conn = httplib.HTTPConnection(self.host + ':' + str(self.port))
    self.conn.request("GET", path)
    self.response = self.conn.getresponse()
    self.log.debug("Reply: %d -%s " % (self.response.status, self.response.reason))
    return self.response.status    
    
  def post(self, path, params):
    """ Post to given path with a params array and return statuscode """
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain", "User-Agent":"gnokii-smsd"}
    params = urllib.urlencode(params)
    self.log.debug('POST: %s:%d%s %s' % (self.host, self.port, path, ''.join(params)))
    conn = httplib.HTTPConnection(self.host + ':' + str(self.port))
    conn.request("POST", path, params, headers)
    self.response = conn.getresponse()
    self.log.debug("Reply: %d -%s " % (self.response.status, self.response.reason))
    return self.response.status


"""This script can run as a daemon or from the console for testing
   For the second casse we attach a console logger to the default file one""" 
if __name__ == "__main__":
    logging.basicConfig(
      level=logging.DEBUG,
      format='%(name)-12s: %(levelname)-8s %(message)s')
    sfw = smsframework()
    # Test incoming
    sms = {'number':'1234567890', 'message':'test message'}
    result = sfw.sms_in(sms)
    if result:
      print "Successfully processed SMS"
    else:
      print "Cannot process SMS"
    # Test outgoing
    sms = sfw.sms_out()
    if sms == None:
      print "No sms received"
    else:
      print "Received SMS"
      print "Number: " + sms['number']
      print "Message: " + sms['message']