#!/usr/bin/env python2.4
# gnokii_smsd settings manager
#
# This will read config files and parse them into settings object
# - defaults.cfg, settings.cfg for general settings (the second one overrides values in the first one)
# - sites.cfg, the site list and sites configuration
#
# For accessing settings from a module:
# - Import this module
#   import config
#
# - Use normal settings functions to retrieve settings from file
#   
#   config.settings.get('section', 'name')
#
# Note: there's no need to pass settings object around as it will be in the global namespace as:
#       config.settings
#

# Hardcoded file names. Please note they're all relative paths
CONFIG_FILE_DEFAULT = 'defaults.cfg'
CONFIG_FILE_CUSTOM = 'settings.cfg'

import ConfigParser
import logging


class SettingsManager(ConfigParser.ConfigParser):
  """ Extended Config Parser with some utility functions """
  def init(self):
    """ Read settings from configuration files """
    self.read([CONFIG_FILE_DEFAULT, CONFIG_FILE_CUSTOM])
  
  """ Logging options """ 
  def log_level(self):
    # Translate level name to numeric
    name = self.get('logging', 'level')
    levels = {
      'CRITICAL': logging.CRITICAL, 
      'ERROR': logging.ERROR,
      'WARNING': logging.WARNING,
      'INFO': logging.INFO,
      'DEBUG': logging.DEBUG,        
    }
    return levels[name]
  
  def log_file(self):
    return self.get('logging', 'file')

# Create settings object and read from file
settings = SettingsManager()
settings.init()


