#!/usr/bin/env python
# encoding: utf-8
"""
gnokii-smsd.py

Created by William White on 2008-07-07.
Copyright (c) 2008 Development Seed. All rights reserved.
"""

import sys, os, time, re, httplib
import logging
import logging.handlers

import config
import smsframework
 
class SMSDaemon(object):
  """ Gateway SMS daemon that will periodically poll gnokii for incoming SMSs """
  def __init__(self):
    self.start_logging()
    self.log = logging.getLogger()    
    # Gateway for incoming messages
    self.incoming = smsframework.smsframework()
    # Gateway for outgoing messages, same
    self.outgoing = self.incoming
    
    self.command = config.settings.get('gnokii', 'command')
    self.params_getsms = config.settings.get('gnokii', 'params_getsms')
    self.params_sendsms = config.settings.get('gnokii', 'params_sendsms')
    self.interval = config.settings.getint('main', 'sms_poll_interval')
    self.process = False
    
  def main(self):
    """ Start the main process """
    self.process = True;
    
    while self.process:
      try:
        self.log.info('Polling sms phone')
        self.poll_incoming()
        self.poll_outgoing()
        # Finished polling, sleep for a while
        time.sleep(self.interval)
      except KeyboardInterrupt, e:
        self.log.info('Keyboard interrupt')
        self.exit()
        break
      except Exception, e:
        self.log.exception('Error in processing.')
        break
      
    self.exit()      

  def poll_incoming(self, max = 1):
    """ Check for new sms messages and process all of them """
    sms = self.get_sms()
    number = 0
    while number < max and sms != None:
      self.log.debug('Got sms from %s: %s' % (sms['number'], sms['message']))
      result = self.incoming.sms_in(sms)
      if result:
        self.log.info('SMS processed:')
      else:
        self.log.warning('Error processing incoming SMS')
      # Commented out for testing, this may cause continuous polling if messages are not sent
      number += 1
      sms = self.get_sms()

  def poll_outgoing(self, max = 1):
    self.log.debug("Fetching outgoing from server...")
    number = 0
    sms = self.outgoing.sms_out()
    while number < max and sms != None:
      self.send_sms(sms)
      number += 1
      sms = self.outgoing.sms_out()
          
  def get_sms(self):
    """ Get sms from inbox if any, and returns it as a dictionary with number and message """
    status = os.popen(self.command + ' ' + self.params_getsms, 'r')
    lines = status.readlines()
    if lines:
      self.log.debug('gnokii returned: ' + ' '.join(lines))
      # Both SMS and sender may have a + prefix
      reg = re.compile("Sender: ([\+\d]+) Msg Center: ([\+\d]+)")
      matches = reg.match(lines[2])
      if matches:
        sms = {}
        sms['number'] = matches.group(1)
        sms['message'] = lines[4]
        return sms
      else:
        # Matching filed, this may be worth logging
        self.log.warning('gnokii response no match: ' + lines[2])
    # Default return value if no messages
    return None
  
  def send_sms(self, sms):
    """ Send sms through gnokii """
    message = sms['message']
    number = sms['number']
    self.log.debug('Sending out sms to %d: %s' % (int(number), message))
    status = os.popen('echo ' + message + ' | ' + self.command + ' ' + self.params_sendsms  + ' ' + str(number), 'r')  
    lines = status.readlines()
    self.log.info("Sending with gnokii: " + ' '.join(lines))
    
  def start_logging(self):
    """ Start a rotating file handler logger """
    logger = logging.getLogger()
    logger.setLevel(config.settings.log_level())
    # Add RotatingFileHander to logger
    handler = logging.handlers.RotatingFileHandler(config.settings.log_file(), 'a+', config.settings.getint('logging', 'file_size'), config.settings.getint('logging', 'file_number'))
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
  
  def exit(self):
    self.process = False
    sys.exit(0)
    
"""This script can run as a daemon or from the console
   For the second casse we attach a console logger to the default file one""" 
if __name__ == "__main__":
    logging.basicConfig(
      level=logging.INFO,
      format='%(name)-12s: %(levelname)-8s %(message)s')
    daemon = SMSDaemon()
    daemon.main()

