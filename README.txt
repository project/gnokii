
Description
===========
Allows gnokii to be used as a two-way SMS gateway. Includes a Python daemon
that manages the process of sending and receiving messages.

Installation
============
- Install and configure gnokii (http://www.gnokii.org/).
- Place the gnokii directory into your modules directory.
- Move the gnokii_smsd directory to a location outside of your web root. This
  can be on an entirely different system as the daemon communicates with Drupal
  using HTTP requests.
- Configure gnokii_smsd (refer to settings-sample.txt).

Sprouted at Development Seed (http://www.developmentseed.org)